from django.conf.urls import url

from .views import (
    ProductListView,
    ProductDetailSlugView,
    ReviewList,
    save_rating
)

urlpatterns = [
    # url(r'^$', ProductListView.as_view(), name='list'),
    url(r'^(?P<slug>[\w-]+)/$', ProductDetailSlugView.as_view(), name='detail'),
    url(r'^review/(?P<slug>[\w-]+)/$', ReviewList, name='product_review'),
    url(r'^add_product_revew', save_rating, name='save_product_rating')
]
