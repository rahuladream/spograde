from django.contrib.sitemaps import Sitemap
from .models import Product
from django.contrib.sites.models import Site
from spograde.config_settings import DOMAIN_URL


class ProductSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.8

    def get_urls(self, site=None, **kwargs):
        site = Site(domain=DOMAIN_URL, name=DOMAIN_URL)
        return super(ProductSitemap, self).get_urls(site=site, **kwargs)

    def items(self):
        return Product.objects.filter(is_digital=0)

    def lastmod(self, obj):
        return obj.timestamp
