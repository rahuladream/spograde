# from django.views import ListView
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.views.generic import ListView, DetailView, View
from django.shortcuts import render, get_object_or_404, redirect
from analytics.mixins import ObjectViewedMixin
from django.http import HttpResponse, JsonResponse
from carts.models import Cart
from django.contrib.auth.decorators import login_required

from .models import Product, ProductFile, Category, Review

import os, math
from wsgiref.util import FileWrapper  # this used in django
from mimetypes import guess_type

from django.conf import settings
from orders.models import ProductPurchase

from spograde.constant import *


def category_view(request, slug):
    try:
        category = Category.objects.get(slug=slug)
    except Category.DoesNotExist:
        raise Http404("Not Found")

    category_products = Product.objects.filter(category=category, active=True)

    context = {
        'slug': slug.upper() + "S",
        'category_products': category_products
    }
    return render(request, "category/index.html", context)


class ProductFeaturedListView(ListView):
    template_name = "products/list.html"

    def get_queryset(self, *args, **kwargs):
        request = self.request
        return Product.objects.all().featured()


class ProductFeaturedDetailView(ObjectViewedMixin, DetailView):
    queryset = Product.objects.all().featured()
    template_name = "products/featured-detail.html"

    # def get_queryset(self, *args, **kwargs):
    #     request = self.request
    #     return Product.objects.featured()


class UserProductHistoryView(LoginRequiredMixin, ListView):
    template_name = "products/user-history.html"

    def get_context_data(self, *args, **kwargs):
        context = super(UserProductHistoryView, self).get_context_data(*args, **kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        context['cart'] = cart_obj
        return context

    def get_queryset(self, *args, **kwargs):
        request = self.request
        views = request.user.objectviewed_set.by_model(Product, model_queryset=False)
        return views


class ProductListView(ListView):
    template_name = "products/list.html"

    # def get_context_data(self, *args, **kwargs):
    #     context = super(ProductListView, self).get_context_data(*args, **kwargs)
    #     print(context)
    #     return context

    def get_context_data(self, *args, **kwargs):
        context = super(ProductListView, self).get_context_data(*args, **kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        context['cart'] = cart_obj
        return context

    def get_queryset(self, *args, **kwargs):
        request = self.request
        return Product.objects.all()


def product_list_view(request):
    queryset = Product.objects.all()
    context = {
        'object_list': queryset
    }
    return render(request, "products/list.html", context)


from django.db.models import Avg


@login_required(login_url='/login/')
def ReviewList(request, pk=None, *args, **kwargs):
    slug = kwargs.get('slug')
    product_obj = Product.objects.get(slug=slug)
    review = Review.objects.filter(product=product_obj).order_by('-timestamp')

    if review.count() > 0:
        avg_rating = review.aggregate(Avg('ratings'))['ratings__avg']
    else:
        avg_rating = 0

    context = {
        'title': 'Product Review - Spograde',
        'product': product_obj,
        'reviews': review,
        'total_review': review.count(),
        'average_rating': math.ceil(avg_rating),
        'remaining_rating': 5 - math.ceil(avg_rating),
        'five_star_count': review.filter(ratings=5).count(),
        'five_star_percent': '',
        'four_star_count': review.filter(ratings=4).count(),
        'four_star_percent': '',
        'three_star_count': review.filter(ratings=3).count(),
        'three_star_percent': '',
        'two_star_count': review.filter(ratings=2).count(),
        'two_star_percent': '',
        'one_star_count': review.filter(ratings=1).count(),
        'one_star_percent': '',
    }

    return render(request, 'products/review.html', context=context)


from django.utils.html import strip_tags


def save_rating(request):
    if request.method == "POST":
        # print(request.POST)
        rating = request.POST.get('selected_rating')
        product_id = request.POST.get('product_id')
        message = strip_tags(request.POST.get('message'))
        print(rating, product_id, message)
        if request.is_ajax():
            product_obj = Product.objects.get(slug=product_id)
            try:
                created = Review.objects.create(
                    user=request.user,
                    name=request.user.full_name,
                    ratings=rating,
                    product=product_obj,
                    review=message
                )
                if created:
                    return JsonResponse({"message": "Thank you for your valuable review, We are keep evolving."})
            except Exception as e:
                return HttpResponse(e, status=400, content_type='application/json')


class ProductDetailSlugView(ObjectViewedMixin, DetailView):
    queryset = Product.objects.all()
    template_name = "products/detail.html"

    # need to fix this working problem
    # giving me lot so

    def get_context_data(self, *args, **kwargs):
        context = super(ProductDetailSlugView, self).get_context_data(*args, **kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        request = self.request
        slug = self.kwargs.get('slug')

        try:
            instance = Product.objects.get(slug=slug, active=True)
        except Product.DoesNotExist:
            raise Http404("Not found..")
        except Product.MultipleObjectsReturned:
            qs = Product.objects.filter(slug=slug, active=True)
            instance = qs.first()
        except:
            raise Http404("Uhhmmm ")

        if instance.category.slug == SPECIAL_CATEGORY_NAME:
            context['edition'] = None
            # context['special_price'] = SPECIAL_PRICE
        else:
            context['edition'] = None

        review = Review.objects.filter(product=instance)
        context['object'] = instance
        context['cart'] = cart_obj
        context['is_any_review'] = True if review.count() > 0 else False
        context['review_count'] = review.count()

        if review.count() > 0:
            avg_rating = review.aggregate(Avg('ratings'))['ratings__avg']
        else:
            avg_rating = 0
        context['average_rating'] = math.ceil(avg_rating)
        context['remaining'] = 5 - math.ceil(avg_rating)
        context['reviews'] = review

        return context


class ProductDetailView(ObjectViewedMixin, DetailView):
    # queryset = Product.objects.all()
    template_name = "products/detail.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ProductDetailView, self).get_context_data(*args, **kwargs)
        print(context)
        # context['abc'] = 123
        return context

    def get_object(self, *args, **kwargs):
        request = self.request
        pk = self.kwargs.get('pk')
        instance = Product.objects.get_by_id(pk)
        if instance is None:
            raise Http404("Product doesn't exist")
        return instance

    # def get_queryset(self, *args, **kwargs):
    #     request = self.request
    #     pk = self.kwargs.get('pk')
    #     return Product.objects.filter(pk=pk)


def product_detail_view(request, pk=None, *args, **kwargs):
    # instance = Product.objects.get(pk=pk, featured=True) #id
    # instance = get_object_or_404(Product, pk=pk, featured=True)
    # try:
    #     instance = Product.objects.get(id=pk)
    # except Product.DoesNotExist:
    #     print('no product here')
    #     raise Http404("Product doesn't exist")
    # except:
    #     print("huh?")

    instance = Product.objects.get_by_id(pk)
    related_products = Product.objects.all()
    if instance is None:
        raise Http404("Product doesn't exist")
    # print(instance)
    # qs  = Product.objects.filter(id=pk)

    # #print(qs)
    # if qs.exists() and qs.count() == 1: # len(qs)
    #     instance = qs.first()
    # else:
    #     raise Http404("Product doesn't exist")
    context = {
        'object': instance,
        'related_products': related_products
    }
    return render(request, "products/detail.html", context)
