# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2020-01-23 16:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0025_product_is_digital'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='mrp_price',
            field=models.DecimalField(decimal_places=2, default=0, help_text='MRP Price', max_digits=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='stock',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='product',
            name='price',
            field=models.DecimalField(decimal_places=2, help_text='Offer Price', max_digits=20),
        ),
    ]
