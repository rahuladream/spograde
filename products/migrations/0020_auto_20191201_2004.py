# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2019-12-01 20:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0019_auto_20191126_1902'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='status',
            field=models.CharField(choices=[('top_sale', 'TOP SALE'), ('top_featured', 'TOP FEATURED'), ('sale_off', 'SALE OFF'), ('popular', 'POPULAR'), ('sale', 'SALE'), ('new', 'NEW')], max_length=100),
        ),
    ]
