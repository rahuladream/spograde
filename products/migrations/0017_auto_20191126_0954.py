# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2019-11-26 09:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0016_auto_20191126_0848'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='product_featured',
        ),
        migrations.AddField(
            model_name='product',
            name='status',
            field=models.IntegerField(choices=[('top_sale', 'TOP SALE'), ('top_featured', 'TOP FEATURED'), ('sale_off', 'SALE OFF'), ('popular', 'POPULAR'), ('new', 'NEW')], default=0, max_length=100),
            preserve_default=False,
        ),
    ]
