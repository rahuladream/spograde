from django.contrib import admin

from .models import Product, ProductFile, Category, Tag, Review, ProductType, ProductSize
from django_summernote.admin import SummernoteModelAdmin


class ProductFileInline(admin.TabularInline):
    model = ProductFile
    extra = 1


class ProductTypeAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'mrp_price', 'offer_price']
    prepopulated_fields = {'slug': ('title',)}


admin.site.register(ProductType, ProductTypeAdmin)


class ProductSizeAdmin(admin.ModelAdmin):
    list_display = ['__str__']
    prepopulated_fields = {'slug': ('title',)}


admin.site.register(ProductSize, ProductSizeAdmin)


class ProductAdmin(SummernoteModelAdmin):
    list_display = ['__str__', 'slug', 'is_offer', 'price']
    prepopulated_fields = {'slug': ('title',)}
    summernote_fields = ('description', 'offer_description',)
    inlines = [ProductFileInline]
    search_fields = ['__str__']

    class Meta:
        model = Product


admin.site.register(Product, ProductAdmin)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'slug']
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Category, CategoryAdmin)


class TagAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'slug']
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Tag, TagAdmin)


class ReviewAdmin(admin.ModelAdmin):
    list_display = ['user', 'product', 'ratings', 'active']


admin.site.register(Review, ReviewAdmin)
