import random
import os
from django.conf import settings
from django.core.files.storage import FileSystemStorage

from django.db import models
from django.db.models import Q
from django.db.models.signals import pre_save, post_save
from django.urls import reverse
from spograde.utils import unique_slug_generator, get_filename
from accounts.models import User


def get_filename_ext(filepath):
    base_name = os.path.basename(filepath)
    name, ext = os.path.splitext(base_name)
    return name, ext


def upload_image_path(instance, filename):
    # print(instance)
    # print(filename)
    new_filename = random.randint(1, 3910209312)
    name, ext = get_filename_ext(filename)
    final_filename = '{new_filename}{ext}'.format(new_filename=new_filename, ext=ext)
    return "products/{new_filename}/{final_filename}".format(
        new_filename=new_filename,
        final_filename=final_filename
    )


class ProductQuerySet(models.query.QuerySet):
    def active(self):
        return self.filter(active=True)

    def featured(self):
        return self.filter(featured=True, active=True)

    def search(self, query):
        lookups = (Q(title__icontains=query) |
                   Q(description__icontains=query) |
                   Q(price__icontains=query) |
                   Q(tag__title__icontains=query)
                   )
        # tshirt, t-shirt, t shirt, red, green, blue,
        return self.filter(lookups).distinct()


class ProductManager(models.Manager):
    def get_queryset(self):
        return ProductQuerySet(self.model, using=self._db)

    def all(self):
        return self.get_queryset().active()

    def featured(self):  # Product.objects.featured()
        return self.get_queryset().featured()

    def get_by_id(self, id):
        qs = self.get_queryset().filter(id=id)  # Product.objects == self.get_queryset()
        if qs.count() == 1:
            return qs.first()
        return None

    def search(self, query):
        return self.get_queryset().active().search(query)


class Tag(models.Model):
    name = models.CharField(max_length=32, db_index=True)
    slug = models.SlugField(max_length=32, db_index=True)

    def get_absolute_url(self):
        return f"/tag/{self.slug}"

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=32, db_index=True)
    slug = models.SlugField(max_length=32, db_index=True, unique=True)

    class Meta:
        verbose_name_plural = "Product Categories"

    def get_absolute_url(self):
        return f"/category/{self.slug}"

    def __str__(self):
        return self.name


RATING = (
    (5, "Excellent"),
    (4, "Good"),
    (3, "Average"),
    (2, "Bad"),
    (1, "Worst")
)

FEATURED = (
    ("top_sale", "TOP SALE"),
    ("top_featured", "TOP FEATURED"),
    ("sale_off", "SALE OFF"),
    ("popular", "POPULAR"),
    ("sale", "SALE"),
    ("new", "NEW"),
)


class ProductType(models.Model):
    title = models.CharField(max_length=120, unique=True)
    slug = models.SlugField(max_length=120, unique=True)
    short_name_to_display = models.CharField(max_length=20, null=True, blank=True)
    mrp_price = models.DecimalField(decimal_places=2, max_digits=10, help_text="New Bat Type MRP Price")
    offer_price = models.DecimalField(decimal_places=2, max_digits=10, help_text="New Bat Type offer Price")

    def __str__(self):
        return self.title


class ProductSize(models.Model):
    title = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=100, unique=True)
    short_name_to_display = models.CharField(max_length=10, null=True, blank=True, default=None)

    def __str__(self):
        return self.title


class Product(models.Model):
    title = models.CharField(max_length=120)
    slug = models.SlugField(blank=True, unique=True)
    description = models.TextField()
    short_description = models.CharField(max_length=50)
    long_description = models.CharField(max_length=500)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    p_type = models.ManyToManyField(ProductType, help_text="Product Type if any (generally ball type)", null=True,
                                    blank=True)
    p_size = models.ManyToManyField(ProductSize, help_text="Product Size if any (generally bat size)", null=True,
                                    blank=True)
    status = models.CharField(choices=FEATURED, max_length=100)
    mrp_price = models.DecimalField(decimal_places=2, max_digits=10, help_text="MRP Price")
    price = models.DecimalField(decimal_places=2, max_digits=20, help_text="Offer Price")
    image = models.ImageField(upload_to=upload_image_path)
    featured = models.BooleanField(default=False)
    active = models.BooleanField(default=True)
    stock = models.IntegerField()
    tags = models.ManyToManyField(Tag)
    timestamp = models.DateTimeField(auto_now_add=True)
    is_offer = models.BooleanField(default=False)
    offer_description = models.TextField(null=True, blank=True)
    is_digital = models.BooleanField(default=False)

    objects = ProductManager()

    def get_absolute_url(self):
        # return "/products/{slug}/".format(slug=self.slug)
        return reverse("products:detail", kwargs={"slug": self.slug})

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.title

    @property
    def name(self):
        return self.title

    def get_downloads(self):
        qs = self.productfile_set.all()
        return qs


def product_pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(product_pre_save_receiver, sender=Product)


def upload_product_file_loc(instance, filename):
    slug = instance.product.slug
    # id_ = 0
    id_ = instance.id
    if id_ is None:
        Klass = instance.__class__
        qs = Klass.objects.all().order_by('-pk')
        if qs.exists():
            id_ = qs.first().id + 1
        else:
            id_ = 0
    if not slug:
        slug = unique_slug_generator(instance.product)
    location = "product/{slug}/{id}/".format(slug=slug, id=id_)
    return location + filename  # "path/to/filename.mp4"


class Review(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(max_length=300, blank=True, null=True)
    product = models.ForeignKey(Product)
    ratings = models.IntegerField(choices=RATING, help_text="Must be between 1 - 5")
    review = models.CharField(max_length=600)
    active = models.BooleanField(default=True)
    image = models.ImageField(upload_to="review/%m/%d", blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    @property
    def display_name(self):
        return self.user.name

    def __str__(self):
        return str(self.ratings)


class ProductFile(models.Model):
    product = models.ForeignKey(Product)
    name = models.CharField(max_length=120, null=True, blank=True)
    file = models.FileField(
        upload_to=upload_product_file_loc)  # path

    # filepath        = models.TextField() # '/protected/path/to/the/file/myfile.mp3'
    def __str__(self):
        return str(self.file.name)

    @property
    def display_name(self):
        og_name = get_filename(self.file.name)
        if self.name:
            return self.name
        return og_name

    def get_default_url(self):
        return self.product.get_absolute_url()

    def generate_download_url(self):
        bucket = getattr(settings, 'AWS_STORAGE_BUCKET_NAME')
        region = getattr(settings, 'S3DIRECT_REGION')
        access_key = getattr(settings, 'AWS_ACCESS_KEY_ID')
        secret_key = getattr(settings, 'AWS_SECRET_ACCESS_KEY')
        if not secret_key or not access_key or not bucket or not region:
            return "/product-not-found/"
        PROTECTED_DIR_NAME = getattr(settings, 'PROTECTED_DIR_NAME', 'protected')
        path = "{base}/{file_path}".format(base=PROTECTED_DIR_NAME, file_path=str(self.file))
        aws_dl_object = AWSDownload(access_key, secret_key, bucket, region)
        file_url = aws_dl_object.generate_url(path, new_filename=self.display_name)
        return file_url

    def get_download_url(self):  # detail view
        return reverse("products:download",
                       kwargs={"slug": self.product.slug, "pk": self.pk}
                       )
