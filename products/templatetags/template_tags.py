from django.template import Library

register = Library()


@register.filter(name='average')
def average(all_review, count_review):
    if count_review == 0:
        return 0
    else:
        temp = 0
        for review in all_review:
            temp += review['ratings']
        avg_rating = (temp // count_review)
        return avg_rating
