from django.contrib import admin
from .models import PaytmHistory


# Register your models here.


class PaytmAdmin(admin.ModelAdmin):
    list_display = ['order', 'user', 'RESPCODE', 'CURRENCY', 'TXNAMOUNT', 'TXNDATE']
    list_filter = ['TXNDATE', 'STATUS', 'TXNDATE']
    search_fields = ['order', 'user']


admin.site.register(PaytmHistory, PaytmAdmin)
