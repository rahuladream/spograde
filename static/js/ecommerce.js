$(document).ready(function(){
    // Contact Form Handler

    var contactForm = $(".contact-form")
    var contactFormMethod = contactForm.attr("method")
    var contactFormEndpoint = contactForm.attr("action") // /abc/
    
    
    function displaySubmitting(submitBtn, defaultText, doSubmit){
      if (doSubmit){
        submitBtn.addClass("disabled")
        submitBtn.html("<i class='fa fa-spin fa-spinner'></i> Sending...")
      } else {
        submitBtn.removeClass("disabled")
        submitBtn.html(defaultText)
      }
      
    }
    contactForm.submit(function(event){
      event.preventDefault()

      var contactFormSubmitBtn = contactForm.find("[type='submit']")
      var contactFormSubmitBtnTxt = contactFormSubmitBtn.text()


      var contactFormData = contactForm.serialize()
      var thisForm = $(this)
      displaySubmitting(contactFormSubmitBtn, "", true)
      $.ajax({
        method: contactFormMethod,
        url:  contactFormEndpoint,
        data: contactFormData,
        success: function(data){
          contactForm[0].reset()
          var toast = new iqwerty.toast.Toast();
          toast.setText(data.message).show();
          setTimeout(function(){
            displaySubmitting(contactFormSubmitBtn, contactFormSubmitBtnTxt, false)
          }, 500)
        },
        error: function(error){
          console.log(error.responseJSON)
          var jsonData = error.responseJSON
          var msg = ""

          $.each(jsonData, function(key, value){ // key, value  array index / object
            msg += key + ": " + value[0].message + "<br/>"
          })

          var toast = new iqwerty.toast.Toast();
          toast.setText(msg).show();

          setTimeout(function(){
            displaySubmitting(contactFormSubmitBtn, contactFormSubmitBtnTxt, false)
          }, 500)

        }
      })
    })


    var reviewForm = $(".review-form")
    var reviewFormMethod = reviewForm.attr("method")
    var reviewFormEndpoint = reviewForm.attr("action") // /abc/
    function displaySubmitting(submitBtn, defaultText, doSubmit){
      if (doSubmit){
        submitBtn.addClass("disabled")
        submitBtn.html("Sending...")
      } else {
        submitBtn.removeClass("disabled")
        submitBtn.html(defaultText)
      }
      
    }
    reviewForm.submit(function(event){
      event.preventDefault()

      var reviewFormSubmitBtn = reviewForm.find("[type='submit']")
      var reviewFormSubmitBtnTxt = reviewFormSubmitBtn.text()


      var reviewFormData = reviewForm.serialize()
      var thisForm = $(this)
      displaySubmitting(reviewFormSubmitBtn, "", true)
      $.ajax({
        method: reviewFormMethod,
        url:  reviewFormEndpoint,
        data: reviewFormData,
        success: function(data){
          reviewForm[0].reset()
          var toast = new iqwerty.toast.Toast();
          toast.setText(data.message).show();
          setTimeout(location.reload(),  400);
          setTimeout(function(){
            displaySubmitting(reviewFormSubmitBtn, reviewFormSubmitBtnTxt, false)
          }, 500)
        },
        error: function(error){
          console.log(error.responseText)
          var jsonData = error.responseJSON
          var toast = new iqwerty.toast.Toast();
          toast.setText('Something went missing, Please try again after sometime').show();
          setTimeout(function(){
            displaySubmitting(reviewFormSubmitBtn, reviewFormSubmitBtnTxt, false)
          }, 500)

        }
      })
    })

    var subscribeForm = $(".subscribe-form")
    var subscribeFormMethod = subscribeForm[1].method;
    var subscribeFormEndpoint = subscribeForm[1].action; // /abc/
    function displaySubmitting(submitBtn, defaultText, doSubmit){
      if (doSubmit){
        submitBtn.addClass("disabled")
        submitBtn.html("Sending...")
      } else {
        submitBtn.removeClass("disabled")
        submitBtn.html(defaultText)
      }
      
    }
    subscribeForm.submit(function(event){
      event.preventDefault()

      var subscribeFormSubmitBtn = subscribeForm.find("[type='submit']")
      var subscribeFormSubmitBtnTxt = subscribeFormSubmitBtn.text()

      var subscribeFormData = subscribeForm.serialize()
      var thisForm = $(this)
      displaySubmitting(subscribeFormSubmitBtn, "", true)
      $.ajax({
        method: subscribeFormMethod,
        url:  subscribeFormEndpoint,
        data: subscribeFormData,
        success: function(data){
          subscribeForm[0].reset()
          var toast = new iqwerty.toast.Toast();
          toast.setText(data.message).show();
          setTimeout(location.reload(),  400);
          setTimeout(function(){
            displaySubmitting(subscribeFormSubmitBtn, subscribeFormSubmitBtnTxt, false)
          }, 500)
        },
        error: function(error){
          var jsonData = error.responseJSON
          var toast = new iqwerty.toast.Toast();
          toast.setText('Something went missing, Please try again after sometime').show();
          setTimeout(function(){
            displaySubmitting(subscribeFormSubmitBtn, "<i class='icon-arrow-right'></i>", false)
          }, 500)
        }
      })
    })


    var newsletterForm = $(".newslettermodal-content-form")
    var newsletterFormMethod = newsletterForm.attr("method")
    var newsletterFormEndpoint = newsletterForm.attr("action") // /abc/
    function displaySubmitting(submitBtn, defaultText, doSubmit){
      if (doSubmit){
        submitBtn.addClass("disabled")
        submitBtn.html("Sending...")
      } else {
        submitBtn.removeClass("disabled")
        submitBtn.html(defaultText)
      }
      
    }
    newsletterForm.submit(function(event){
      event.preventDefault()

      var newsletterFormSubmitBtn = newsletterForm.find("[type='submit']")
      var newsletterFormSubmitBtnTxt = newsletterFormSubmitBtn.text()


      var newsletterFormData = newsletterForm.serialize()
      var thisForm = $(this)
      displaySubmitting(newsletterFormSubmitBtn, "", true)
      $.ajax({
        method: newsletterFormMethod,
        url:  newsletterFormEndpoint,
        data: newsletterFormData,
        success: function(data){
          newsletterForm[0].reset()
          var toast = new iqwerty.toast.Toast();
          console.log(data)
          toast.setText(data.message).show();
          setTimeout(location.reload(),  400);
          setTimeout(function(){
            displaySubmitting(newsletterFormSubmitBtn, newsletterFormSubmitBtnTxt, false)
          }, 500)
        },
        error: function(error){
          console.log(error.responseText)
          var jsonData = error.responseJSON
          var toast = new iqwerty.toast.Toast();
          toast.setText('Something went missing, Please try again after sometime').show();
          setTimeout(function(){
            displaySubmitting(newsletterFormSubmitBtn, newsletterFormSubmitBtnTxt, false)
          }, 500)

        }
      })
    })
    
    jQuery(document).ready(function($){
	    
      $(".btnrating").on('click',(function(e) {
      
      var previous_value = $("#selected_rating").val();
      
      var selected_value = $(this).attr("data-attr");
      $("#selected_rating").val(selected_value);
      
      $(".selected-rating").empty();
      $(".selected-rating").html(selected_value);
      
      for (i = 1; i <= selected_value; ++i) {
      $("#rating-star-"+i).toggleClass('btn-warning');
      $("#rating-star-"+i).toggleClass('btn-default');
      }
      
      for (ix = 1; ix <= previous_value; ++ix) {
      $("#rating-star-"+ix).toggleClass('btn-warning');
      $("#rating-star-"+ix).toggleClass('btn-default');
      }
      
      }));		
    });



    // Auto Search
    var searchForm = $(".search-form")
    var searchInput = searchForm.find("[name='q']") // input name='q'
    var typingTimer;
    var typingInterval = 500 // .5 seconds
    var searchBtn = searchForm.find("[type='submit']")
    searchInput.keyup(function(event){
      // key released
      clearTimeout(typingTimer)

      typingTimer = setTimeout(perfomSearch, typingInterval)
    })

    searchInput.keydown(function(event){
      // key pressed
      clearTimeout(typingTimer)
    })

    function displaySearching(){
      searchBtn.addClass("disabled")
      searchBtn.html("<i class='fa fa-spin fa-spinner'></i> Searching...")
    }

    function perfomSearch(){
      displaySearching()
      var query = searchInput.val()
      setTimeout(function(){
        window.location.href='/search/?q=' + query
      }, 1000)
      
    }


  // Cart + Add Products 
  var productForm = $(".form-product-ajax") // #form-product-ajax

  function getOwnedProduct(productId, submitSpan){
    var actionEndpoint = '/orders/endpoint/verify/ownership/'
    var httpMethod = 'GET'
    var data = {
      product_id: productId
    }

    var isOwner;
    $.ajax({
        url: actionEndpoint,
        method: httpMethod,
        data: data,
        success: function(data){
          console.log(data)
          console.log(data.owner)
          if (data.owner){
            isOwner = true
            submitSpan.html("<a class='btn btn-warning' href='/library/'>In Library</a>")
          } else {
            isOwner = false
          }
        },
        error: function(erorr){
          console.log(error)

        }
    })
    return isOwner
    
  }

  $.each(productForm, function(index, object){
    var $this = $(this)
    var isUser = $this.attr("data-user")
    var submitSpan = $this.find(".submit-span")
    var productInput = $this.find("[name='product_id']")
    var productId = productInput.attr("value")
    var productIsDigital = productInput.attr("data-is-digital")
    
    if (productIsDigital && isUser){
      var isOwned = getOwnedProduct(productId, submitSpan)
    }
  })  


  productForm.submit(function(event){
      event.preventDefault();
      // console.log("Form is not sending")
      var thisForm = $(this)
      // var actionEndpoint = thisForm.attr("action"); // API Endpoint
      var actionEndpoint = thisForm.attr("data-endpoint")
      var httpMethod = thisForm.attr("method");
      var formData = thisForm.serialize();

      $.ajax({
        url: actionEndpoint,
        method: httpMethod,
        data: formData,
        success: function(data){
          var submitSpan = thisForm.find(".submit-span")
          if (data.added){
            submitSpan.html("<div class='btn-group'><a class='btn btn-link' href='/cart/'><i class='icon icon-shopping-cart'></i> In cart</a> <button type='submit' class='btn btn-link'><i class='icon icon-remove'></i> Remove?</button></div>")
            var toast = new iqwerty.toast.Toast();
            toast.setText("Product added to cart <i class='icon icon-handbag'></i>").show();
            refreshCart();
          } else {
            submitSpan.html("<button type='submit' class='btn'><i class='icon icon-handbag'></i> <span>Add to cart</span></button>")
            var toast = new iqwerty.toast.Toast();
			      toast.setText("Product removed from cart <i class='icon icon-handbag'></i>").show();
            window.location.reload();
            refreshCart();
          }
          var navbarCount = $(".navbar-cart-count")
          navbarCount.text(data.cartItemCount)
          var currentPath = window.location.href

          if (currentPath.indexOf("cart") != -1) {
            refreshCart()
          }
        },
        error: function(errorData){
          $.alert({
            title: "Oops!",
            content: "An error occurred",
            theme: "modern",
          })
        }
      })

  })
  // http://zetcode.com/articles/javascriptjsonurl/
  function refreshCart(){
    console.log("in current cart")
    var cartTable = $(".cart-table")
    var cartBody = cartTable.find(".cart-body")
    cartBody.html("<h2><center> Please wait cart is refreshing... <br/><i style='font-size:58px; margin-top:20px' class='fa fa-spin fa-spinner fa-10x'></i></center></h2>")
    var productRows = cartBody.find(".cart-product")
    var currentUrl = window.location.href

    var refreshCartUrl = '/api/cart/'
    var refreshCartMethod = "GET";
    var data = {};
    $.ajax({
      url: refreshCartUrl,
      method: refreshCartMethod,
      data: data,
      success: function(data){
        
        var hiddenCartItemRemoveForm = $(".cart-item-remove-form")
        if (data.products.length > 0){
            productRows.html(" ")
            i = data.products.length
            console.log(i)
            $.each(data.products, function(index, value){
              console.log(value)
              var newCartItemRemove = hiddenCartItemRemoveForm.clone()
              console.log(newCartItemRemove)
              newCartItemRemove.css("display", "block")
              // newCartItemRemove.removeClass("hidden-class")
              newCartItemRemove.find(".cart-item-product-id").val(value.id)
                // cartBody.append("<tr><th scope=\"row\">" + i + "</th><td><a href='" + value.url + "'>" + value.name + "</a>" + newCartItemRemove.html() + "</td><td>" + value.price + "</td></tr>")
                // cartBody.prepend("<div class='cart-table-prd cart-product'><div class='cart-table-prd-image'><a href='" + value.url + "'><img src='" + value.image + "'></a></div><div class='cart-table-prd-name'><h5><a href=''> " + value.name + " </a></h5><h2><a href='" +value.url+ "'>" + value.name + "</a></h2></div> \
                // <div class='cart-table-prd-qty'><span>qty:</span> <b>1</b></div><div class='cart-table-prd-price'><span>price:</span> <b>&#8377; " + value.price + "</b></div><div class='cart-table-prd-action'>" + newCartItemRemove.html() + "</div></div>")
                i --
            })
            cartBody.find(".cart-subtotal").text(data.subtotal)
            cartBody.find(".cart-total").text(data.total)
        } else {
          window.location.href = currentUrl
        }
        
      },
      error: function(errorData){
        $.alert({
            title: "Oops!",
            content: "An error occurred",
            theme: "modern",
          })
      }
    })


  }



})
