Hey there,

First off, I’d like to extend a warm welcome and thank you for subscribing to the Spograde LLP newsletter. I recognize that your time is valuable and I’m seriously flattered that you chose to join us.

If you have any questions, just reply to this email — we're always happy to help out.

Cheers, Spograde LLP