from django.conf.urls import url
from .views import (
    ProductVerificationView
)

urlpatterns = [
    url(r'^product-verification/', ProductVerificationView.as_view(), name='product-verification'),
]
