from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from django import forms
from verification.models import Verification


class ProductVerificationForm(forms.Form):
    security_code = forms.CharField(label='Securiy Code', widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': "Please enter 16 digit securiyt code"}), max_length=16,
                                    min_length=16)

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super(ProductVerificationForm, self).__init__(*args, **kwargs)

    def clean(self):
        request = self.request
        data = self.cleaned_data
        security_code = data.get('security_code')
        print(security_code)
        verification_obj = Verification.objects.filter(security_code=security_code)

        if verification_obj.exists():
            not_active = verification_obj.filter(if_flagged=True)
            if not_active.exists():
                msg1 = "Your Product is Marked as Flagged. Hence We can not verify your product. Please contact us to resolve the matter."
                raise forms.ValidationError(mark_safe(msg1))
        else:
            msg2 = "The Authorization code you entered is not valid. Try Again"
            raise forms.ValidationError(mark_safe(msg2))

        return data
