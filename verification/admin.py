from django.contrib import admin
from .models import Verification, Faq
import csv, datetime
from io import StringIO
from django.http import HttpResponse
from django_summernote.admin import SummernoteModelAdmin


# Register your models here.

def export_csv(modeadmin, request, queryset):
    security_code_hypothetical = StringIO()
    security_writer = csv.writer(security_code_hypothetical)
    security_writer.writerow(['ID', 'CODE'])

    for _ in queryset:
        security_writer.writerow([_.pk, _.security_code])

    security_code_hypothetical.seek(0)
    response = HttpResponse(security_code_hypothetical, content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=security_code_{}.csv'.format(datetime.datetime.now())
    export_csv.short_description = "Export Security as File"
    return response


def make_flagged(modeladmin, request, queryset):
    queryset.update(if_flagged=True)
    make_flagged.short_description = "Mark Security Code as flagged"


def make_unflagged(modeadmin, request, queryset):
    queryset.update(if_flagged=False)
    make_unflagged.short_description = "Mark Security Code as Unflagged"


def create_bulk(self, request, queryset):
    for _ in range(int(len(queryset))):
        Verification.objects.create()
    self.message_user(request, "{} succesfully Security Code Generated".format(int(len(queryset))))
    create_bulk.short_description = "Bulk Security Code Generate"


class FAQAdmin(SummernoteModelAdmin):
    field = ['question', 'is_active']
    summernote_fields = ('answer')
    list_filter = ['category', 'is_active']


admin.site.register(Faq, FAQAdmin)


class VerificationAdmin(admin.ModelAdmin):
    field = ['security_code', 'if_flagged']
    list_display = ['security_code', 'if_flagged']
    search_fields = ['security_code']
    list_filter = ['created_at', 'if_flagged']
    actions = [make_flagged, make_unflagged, export_csv, create_bulk]


admin.site.disable_action('delete_selected')
admin.site.register(Verification, VerificationAdmin)
