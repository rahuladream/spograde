from django.db import models
import string, random


def random_string(size=16, chars=string.ascii_uppercase + string.digits):
    """
    Used to generate the random 16 digit string
    """
    return ''.join(random.choice(chars) for _ in range(size))


# Create your models here.
class Verification(models.Model):
    """
    Model will be used for product verification in the entire ecomm
    The basic scene of this model is 
    1. Generate Random Number from Backend (More Specific => Admin)
    2. Use it for product verification
    """
    security_code = models.CharField(max_length=16, default=random_string, unique=True)
    if_flagged = models.BooleanField(default=False, help_text="Make me true if you want to inactive this security")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return self.security_code


RATING = (
    ("top-questions", "Top Questions"),
    ("general_questions", "General Questions")
)


class Faq(models.Model):
    """
    Model is used to create FAQ system for the front end
    """
    question = models.CharField(max_length=500)
    answer = models.TextField()
    category = models.CharField(choices=RATING, max_length=100)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.question
