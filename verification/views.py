from django.shortcuts import render
from spograde.mixins import NextUrlMixin, RequestFormAttachMixin
from django.views.generic import FormView
from .forms import ProductVerificationForm


# Create your views here.

class ProductVerificationView(NextUrlMixin, RequestFormAttachMixin, FormView):
    form_class = ProductVerificationForm
    template_name = 'verification.html'
    success_url = 'success-verification.html'

    def form_valid(self, form):
        context = {
            "form": form
        }
        return render(self.request, 'success_verification.html', context)
