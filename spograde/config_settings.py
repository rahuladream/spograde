LOCAL_IP = '127.0.0.1'

DICT_OF_PRODUCTS = {
    'https://spograde.com/products/spograde-kashmiri-willow-tennis/',
    'https://spograde.com/products/spograde-gully-cricket/',
    'https://spograde.com/products/spograde-kashmiri-willow-leather/'
}

SERVICE_PROVIDER_EMAIL = 'spograde@gmail.com'


DJANGO_ADMIN_HEADERS = "Spograde LLP Administration"
DJANGO_SITE_TITLE = "Spograde LLP"
DJANGO_INDEX_TITLE = "Welcome to Spograde LLP"

DOMAIN_URL = 'domain.com'