from .base import *
from django.conf import settings

def get_apps(app_dict):
    """
    Convert dictionary into array
    """
    app = []
    for key,value in app_dict.items():
        app.append(value)
    return app


def get_app_id(app):
    """
    Get app id using app name
    """
    code = 0
    for key, value in settings.LOCAL_APPS_DICT.items():
        if value == app:
            code = key
            break

    return code