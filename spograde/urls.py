"""spograde URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls.static import static

from django.conf.urls import url, include
# from django.urls import path
from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.views.generic import TemplateView, RedirectView
import os
from django.views.static import serve

from django.contrib.sitemaps.views import sitemap
from .sitemaps import StaticSitemap
from products.sitemaps import ProductSitemap
from django.views.generic import TemplateView

from accounts.views import LoginView, RegisterView, GuestRegisterView
from addresses.views import (
    AddressCreateView,
    AddressListView,
    AddressUpdateView,
    checkout_address_create_view, 
    checkout_address_reuse_view
    )
from analytics.views import SalesView, SalesAjaxView
from billing.views import payment_method_view, payment_method_createview
from carts.views import cart_detail_api_view
from marketing.views import MarketingPreferenceUpdateView, MailchimpWebhookView
from orders.views import LibraryView

from products.views import category_view

from carts.views import payment_response

from verification.views import ProductVerificationView

from .views import home_page, about_page, subscribe, ipl_landing_page, intellectual_property, contact_page, shipping_policy, disclaimer, payment_partners,refund_return_policy,term_of_use,privacy_policy,faq

sitemaps = {
    'pages': StaticSitemap,
    'product': ProductSitemap
}

urlpatterns = [
    # Overall website URL structure
    url(r'^$', home_page, name='home'),
    url('sitemap.xml', sitemap, {'sitemaps' : sitemaps } , name='django.contrib.sitemaps.views.sitemap'),
    url('robots.txt', TemplateView.as_view(template_name="robots.txt", content_type='text/plain')),
    url(r'^about/$', about_page, name='about'),
    url(r'^terms-of-use/$', term_of_use, name='term_of_use'),
    url(r'^privacy-policy/$', privacy_policy, name='privacy_policy'),
    url(r'^return-refund-cancellation-policy/$', refund_return_policy, name='return_refund_policy'),    
    url(r'^faq/$', faq, name='faq'),
    url(r'^intellectual-property/$', intellectual_property, name='intellectual_property'),
    url(r'^shipping-policy/$', shipping_policy, name='shipping_policy'),
    url(r'^disclaimer/$', disclaimer, name='disclaimer'),
    # url(r'^payment-partners/$', payment_partners, name='payment_partners'),

    url(r'^supportyourteam2020/$', ipl_landing_page, name='ipl_landing_page'),

    
    url('summernote/', include('django_summernote.urls')),


    #url(r'^accounts/login/$', RedirectView.as_view(url='/login')),
    url(r'^accounts/$', RedirectView.as_view(url='/account')),
    url(r'^account/', include("accounts.urls", namespace='account')),
    
    # url(r'^', include("verification.urls", namespace='verification')),

    url(r'^accounts/', include("accounts.passwords.urls")),
    url(r'^address/$', RedirectView.as_view(url='/addresses')),
    url(r'^addresses/$', AddressListView.as_view(), name='addresses'),
    url(r'^addresses/create/$', AddressCreateView.as_view(), name='address-create'),
    url(r'^addresses/(?P<pk>\d+)/$', AddressUpdateView.as_view(), name='address-update'),
    url(r'^analytics/sales/$', SalesView.as_view(), name='sales-analytics'),
    url(r'^analytics/sales/data/$', SalesAjaxView.as_view(), name='sales-analytics-data'),
    url(r'^contact/$', contact_page, name='contact'),
    url(r'^subscribe/$', subscribe, name='subscribe'),
    url(r'^login/$', LoginView.as_view(), name='login'),

    url(r'^product-verification/$', ProductVerificationView.as_view(), name='product-verification'),


    url(r'^checkout/address/create/$', checkout_address_create_view, name='checkout_address_create'),
    url(r'^checkout/address/reuse/$', checkout_address_reuse_view, name='checkout_address_reuse'),
    url(r'^register/guest/$', GuestRegisterView.as_view(), name='guest_register'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^api/cart/$', cart_detail_api_view, name='api-cart'),
    url(r'^cart/', include("carts.urls", namespace='cart')),
    
    url(r'^paytm/response/', payment_response, name='payment_response'),

    url(r'^billing/payment-method/$', payment_method_view, name='billing-payment-method'),
    url(r'^billing/payment-method/create/$', payment_method_createview, name='billing-payment-method-endpoint'),
    url(r'^register/$', RegisterView.as_view(), name='register'),
    # url(r'^bootstrap/$', TemplateView.as_view(template_name='bootstrap/example.html')),
    # url(r'^library/$', LibraryView.as_view(), name='library'),

    url(r'^category/(?P<slug>[\w-]+)/$', category_view, name='detail'),
    
    url(r'^orders/', include("orders.urls", namespace='orders')),
    url(r'^products/', include("products.urls", namespace='products')),
    url(r'^search/', include("search.urls", namespace='search')),
    url(r'^settings/$', RedirectView.as_view(url='/account')),
    url(r'^settings/email/$', MarketingPreferenceUpdateView.as_view(), name='marketing-pref'),
    url(r'^webhooks/mailchimp/$', MailchimpWebhookView.as_view(), name='webhooks-mailchimp'),
    url(r'^spograde-admin/', admin.site.urls), # Admin Panel Change according to your configuration
    url(r'^media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT}), 
    url(r'^static/(?P<path>.*)$', serve,{'document_root': settings.STATIC_ROOT}),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
