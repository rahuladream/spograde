from django.contrib.sitemaps import Sitemap
from django.urls import reverse
from django.contrib.sites.models import Site

class StaticSitemap(Sitemap):
    changefreq = 'weekly'
    priority   = 0.8

    def items(self):
        return [
            'home',
            'about',
            'term_of_use',
            'privacy_policy',
            'return_refund_policy',
            'faq',
            'shipping_policy',
            'contact',
            'product-verification'
        ]
    
    def location(self, obj):
        return reverse(obj)