from django.contrib.auth import authenticate, login, get_user_model
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from products.views import ProductListView
from analytics.models import Visitors
import json
from post_office import mail
from django.conf import settings
from products.models import Product, Tag
from spograde.mixins import get_client_ip, get_visitors_detail
from django.core.exceptions import ObjectDoesNotExist
from marketing.models import Contact
from django.db import IntegrityError
from accounts.models import Subscribe
from verification.models import Faq
from .forms import ContactForm
from django.template.loader import get_template
from carts.models import Cart
from spograde.constant import *
from spograde.config_settings import LOCAL_IP, DICT_OF_PRODUCTS, SERVICE_PROVIDER_EMAIL


def cart_detail_api_view(request):
    """
    API to show the cart detail or create new cart every user visit
    """
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    products = [{
        "id": x.id,
        "url": x.get_absolute_url(),
        "image": x.image.url,
        "name": x.name,
        "price": x.price
    }
        for x in cart_obj.products.all()]
    cart_data = {"products": products, "subtotal": cart_obj.subtotal, "total": cart_obj.total}
    return JsonResponse(cart_data)


def home_page(request):
    """
    To Handle the homepage request
    1. fetch the required product and tags
    2. save and request visitor information
    """

    ip_address = get_client_ip(request)

    def get_context_data(self, *args, **kwargs):
        context = super(ProductListView, self).get_context_data(*args, **kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        context['cart'] = cart_obj
        return context

    def get_queryset(self, *args, **kwargs):
        request = self.request
        return Product.objects.all()

    product = Product.objects.all()
    tags = Tag.objects.all()
    top_sale = Product.objects.filter(status="top_sale")
    top_featured = Product.objects.filter(status="top_featured")
    sale_off = Product.objects.filter(status="sale_off")

    """
    Visitor details
    """
    is_mobile = request.user_agent.is_mobile
    is_pc = request.user_agent.is_pc
    is_bot = request.user_agent.is_bot
    browser_detail = {
        'browser': request.user_agent.browser,
        'family': request.user_agent.browser.family,  # returns 'Mobile Safari'
        'version': request.user_agent.browser.version_string,  # returns '5.1',
    }
    os_detail = {
        'os': request.user_agent.os,
        'family': request.user_agent.os.family,
        'version': request.user_agent.os.version_string,
    }
    context = {
        'homepage': True,
        'tags': tags,
        'products': product,
        'top_sales': top_sale,
        'top_featureds': top_featured,
        'sale_offs': sale_off
    }
    device = request.user_agent.device.family  # returns 'iPhone/Android'

    try:
        visitors = Visitors.objects.get(ip_address=ip_address)
    except ObjectDoesNotExist:
        visitors = Visitors.objects.create(ip_address=ip_address,
                                           is_mobile=is_mobile, is_pc=is_pc, is_bot=is_bot,
                                           browser_detail=browser_detail, os_detail=os_detail,
                                           device=device)

    # Exclude the local IP
    if ip_address == LOCAL_IP:
        visitor_detail = 'Local Server has no Details'
    else:
        visitor_detail = get_visitors_detail(ip_address)

    visitors.hit_count = visitors.hit_count + 1
    visitors.visitor_details = visitor_detail
    visitors.save()

    return render(request, "home_page.html", context)


def term_of_use(request):
    context = {
        "title": "Terms of Use"
    }
    return render(request, "term_of_use.html", context)


def privacy_policy(request):
    context = {
        "title": "Policy Policy"
    }
    return render(request, "policy_privacy.html", context)


def faq(request):
    """
    Fetch all the faqs stored in database
    """
    object_faq = Faq.objects.all()
    context = {
        'faqs': object_faq,
        "title": "FAQ Page"
    }
    return render(request, "faq.html", context)


def disclaimer(request):
    context = {

    }
    return render(request, "disclaimer.html", context)


def intellectual_property(request):
    context = {
        # Add your information if any
    }
    return render(request, "intellectual_property.html", context)


def shipping_policy(request):
    context = {
        # Add your information if any
    }
    return render(request, "shipping_policy.html", context)


def payment_partners(request):
    context = {
        # Add your information if any
    }
    return render(request, "payment_partners.html", context)


def about_page(request):
    context = {
        "title": "About Us",
        "content": " Welcome to the about page."
    }
    return render(request, "about_us.html", context)


def refund_return_policy(request):
    context = {
        "title": "Refund, Return and Cancellation Policy"
    }
    return render(request, "return_refund_policy.html", context)


def ipl_landing_page(request):
    """
    New IPL landing page for new offering if any
    Edit your data and function name accordin to the requirement
    """
    cart_obj, new_or_get = Cart.objects.new_or_get(request)
    cart_obj.delete()

    ipl_product = Product.objects.filter(category__slug=SPECIAL_CATEGORY_NAME)

    context = {
        "title": "IP Landing Page",
        'products': ipl_product
    }
    return render(request, "ipl_landing_page.html", context)


def subscribe(request):
    """
    Sending predescribed mail to the subscribed user
    """
    if request.method == "POST":
        email = request.POST.get('email')

        if not email:
            return HttpResponse("Please enter email", status=400, content_type='application/json')
        try:
            p, created = Subscribe.objects.get_or_create(email=email)
            context = {
                'products': DICT_OF_PRODUCTS,
            }
            txt_ = get_template("registration/emails/subscribe.txt").render(context)
            html_ = get_template("registration/emails/subscribe.html").render(context)
            subject = "Thank you for subscribing us!"
            from_email = settings.EMAIL_HOST_USER
            recipient_list = [email]
            mail.send(
                recipient_list,
                from_email,
                subject=subject,
                message=txt_,
                html_message=html_,
                headers={'Reply-to': SERVICE_PROVIDER_EMAIL},
                priority='medium',
            )
            if created:
                return JsonResponse({"message": "Thank you for Subscribing Us."})
            else:
                return JsonResponse({"message": "Already subscribed! Thank you"})
        except IntegrityError as error:
            return JsonResponse({"message": "We associated with an error", "error": str(error)})

    return JsonResponse({'message': 'Please enter valid mail'})


def contact_page(request):
    context = {
        "title": "Contact",
        "content": " Welcome to the contact page.",
        "form": 'form',
    }
    if request.method == "POST":
        fname = request.POST.get('name')
        email = request.POST.get('email')
        msg = request.POST.get('message')

        if not email:
            return HttpResponse("Please enter email", status=400, content_type='application/json')
        try:
            p, created = Contact.objects.get_or_create(fname=fname, email=email, message=msg)
            recipient_list = [settings.EMAIL_HOST_USER]
            mail.send(
                recipient_list,
                email,
                subject='A contact message from {}'.format(email),
                message=msg,
                headers={'Reply-to': email},
                priority='medium',
            )
            if created:
                return JsonResponse({"message": "Thank you for contacting us. We will get back soon"})

            else:
                return JsonResponse({"message": "Something bad happened"})
        except IntegrityError as error:
            return JsonResponse({"message": "We associated with an error", "error": str(error)})
        # return JsonResponse({"message": "Thank your for your submission"})

    return render(request, "contact/view.html", context)
