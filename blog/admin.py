from django.contrib import admin
from .models import *
from django.utils.safestring import mark_safe
from django.utils.html import format_html
from django.template import RequestContext


# Register your models here.


class TagAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(TagBlog, TagAdmin)


class PostAdmin(admin.ModelAdmin):

    def background_images(self, obj):
        return mark_safe('<img src="{url}" width="{width}" height={height} />'.format(
            url=obj.background_image.url,
            width='228',
            height='145',
        )
        )

    readonly_fields = ["background_images"]
    summernote_fields = ('content',)
    list_display = ['title', 'slug', 'author', 'created_on', 'status']
    prepopulated_fields = {'slug': ('title',)}


admin.site.register(Blog, PostAdmin)


class CommentAdmin(admin.ModelAdmin):
    list_display = ['post', 'author', 'email', 'created_at']


admin.site.register(Comment, CommentAdmin)
