#! /bin/bash

# This script will setup following things on server

# - Apt-Fast for fast update and upgrade with 14 threads
# - Virtual Environment
# - Will create two user dev-django1 and devops2
# - Will create a project directory (Only members of devops group can access this directory)
# - Will enable developer to login with dev-django1 and devops2 key
# - Fail2Ban
# - Postgres 9.4+
# - Node 8.11+
# - Nginx
# - SuperVisor
# - Let's Encrypt
# - ZSH

# update the system

echo "updating system....."

sudo apt-get update;
sudo apt-get upgrade -y

sudo apt-get install -y  software-properties-common
# install apt-fast (https://github.com/ilikenwf/apt-fast). Choose the max connections as 14
# Select values in following order
# 1
# 14
# yes
sudo add-apt-repository ppa:apt-fast/stable && sudo apt-get update && sudo apt-get -y install apt-fast

#install pip and python-dev

echo "installing python"
sudo apt-fast install -y python-dev python-pip -y


#install virtual env

echo "installing virtualenv"
sudo pip install virtualenv
#virtual env wrapper

echo "installing virtualenvwrapper"
sudo pip install virtualenvwrapper


#Create Users and developer group(--system is to create a system account)
# We're not going to create dev-django2 and devops1 as they have no role
sudo groupadd --system webapps
sudo useradd --system --gid webapps --shell /bin/bash --home /home/dev-django1 dev-django1

echo "Creating Project Directory Spograde"
sudo mkdir -p /opt/spograde

###### Create devops group####
sudo groupadd devops
# Give devops root access (http://stackoverflow.com/questions/8784761/adding-users-to-sudoers-through-shell-script)
sudo -i
echo '%devops   ALL=(ALL:ALL) ALL' >> /etc/sudoers

# Create DevOps
sudo useradd --gid devops --shell /bin/bash --home /home/devops2 devops2

#Add devops to webapps group(Reference http://www.cyberciti.biz/faq/howto-linux-add-user-to-group/)
#Add existing user tony to ftp supplementary/secondary group with the user-mod command using the -a option ~ i.e.
# add the user to the supplemental group(s). Use only with -G option:
#usermod -a -G <group_name> <userid>

usermod -a -G webapps devops2

Press CTRL+D # Basically this will run `exit` command, we could write direct command here but it is changing pycharm
# theme color to white which makes our code unreadable hence we will press ctrl+D instead of running exit command

#make home directories
sudo mkdir /home/dev-django1
sudo mkdir /home/devops2

sudo -i


#change the ownership to the users
chown -R dev-django1:webapps /home/dev-django1

chown -R devops2:devops /home/devops2

Press CTRL+D

#change the permission
sudo chmod -R 700 /home/dev-django1
sudo chmod -R 700 /home/devops2

#check the addition
id devops2
id dev-django1

#Create passwords


echo "enter password for devops2:"
sudo passwd devops2

echo "enter password for dev-django1:"
sudo passwd dev-django1

# Now add keys in server

sudo su devops2
cd ~
mkdir .ssh
cd .ssh
touch authorized_keys

#and paste the content of .pub file generated using keygen command
chmod 600 ~/.ssh/authorized_keys

Press CTRL+D


sudo su dev-django1
cd ~
mkdir .ssh
cd .ssh
touch authorized_keys

#and paste the content of .pub file generated using keygen command
chmod 600 ~/.ssh/authorized_keys

Press CTRL+D


#Take backup
sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config.factory-defaults
sudo chmod a-w /etc/ssh/sshd_config.factory-defaults


echo "putting new content in sshd_config"

# If this doesn't work which will HAPPEN than copy the content of echo in mentioned file

echo "# Package generated configuration file
# See the sshd_config(5) manpage for details

# What ports, IPs and protocols we listen for
Port 22
# Use these options to restrict which interfaces/protocols sshd will bind to
#ListenAddress ::
#ListenAddress 0.0.0.0
Protocol 2
# HostKeys for protocol version 2
HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_dsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key
#Privilege Separation is turned on for security
UsePrivilegeSeparation yes

# Lifetime and size of ephemeral version 1 server key
KeyRegenerationInterval 3600
ServerKeyBits 1024

# Logging
SyslogFacility AUTH
LogLevel VERBOSE

# Authentication:
LoginGraceTime 120
PermitRootLogin no
StrictModes yes

RSAAuthentication yes
PubkeyAuthentication yes
#AuthorizedKeysFile     %h/.ssh/authorized_keys

# Don't read the user's ~/.rhosts and ~/.shosts files
IgnoreRhosts yes
# For this to work you will also need host keys in /etc/ssh_known_hosts
RhostsRSAAuthentication no
# similar for protocol version 2
HostbasedAuthentication no
# Uncomment if you don't trust ~/.ssh/known_hosts for RhostsRSAAuthentication

#IgnoreUserKnownHosts yes

# To enable empty passwords, change to yes (NOT RECOMMENDED)
PermitEmptyPasswords no

# Change to yes to enable challenge-response passwords (beware issues with
# some PAM modules and threads)
ChallengeResponseAuthentication no

# Change to no to disable tunnelled clear text passwords
PasswordAuthentication no

# Kerberos options
#KerberosAuthentication no
#KerberosGetAFSToken no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes

# GSSAPI options
#GSSAPIAuthentication no
#GSSAPICleanupCredentials yes


X11Forwarding yes
X11DisplayOffset 10
PrintMotd no
PrintLastLog yes
TCPKeepAlive yes
#UseLogin no

#MaxStartups 10:30:60
#Banner /etc/issue.net

# Allow client to pass locale environment variables
AcceptEnv LANG LC_*

Subsystem sftp /usr/lib/openssh/sftp-server

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the ChallengeResponseAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via ChallengeResponseAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and ChallengeResponseAuthentication to 'no'.
UsePAM yes
#
# Google Compute Engine times out connections after 10 minutes of inactivity.
# Keep alive ssh connections by sending a packet every 2 minutes.
ClientAliveInterval 120

# Prevent reverse DNS lookups.
UseDNS no


AllowUsers dev-django1 devops2
" > /etc/ssh/sshd_config

sudo systemctl restart sshd.service
sudo systemctl restart ssh.service

# Restart SSH server so that settings can take effect
echo "Installing fail2ban..."
sudo apt-fast update;
sudo apt-fast -y install fail2ban


#set the correct permissions for the developer group
# so that devops Group can work in there
sudo chgrp -R devops /opt/spograde

#dev-django1 is the owner and files belong to the devops group
sudo chown -R dev-django1:devops /opt/spograde

#so that developers can read/write/execute, others can read(2775 is for others can )
sudo chmod -R 2775 /opt/spograde
sudo chmod -R 775 /opt/spograde

# Now install basic software to run our project

echo "update system..."
sudo apt-fast update


echo "Installing postgres 9.4.X +..."

sudo apt-fast install postgresql postgresql-contrib

sudo apt-fast install postgresql
sudo apt-fast install python-psycopg2
sudo apt-fast install libpq-dev


sudo systemctl enable postgresql.service
sudo systemctl restart postgresql.service

echo "Installing node js 8.11.X + ..."

cd ~
curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh

sudo bash nodesource_setup.sh
sudo apt-fast install nodejs

echo "Installing build-essential..."
sudo apt-fast install build-essential

sudo apt-fast install python3-dev python3-pip python3-cffi libcairo2 libpango-1.0-0 libpangocairo-1.0-0 libgdk-pixbuf2.0-0 libffi-dev shared-mime-info

# Install Nginx

sudo apt-fast update
sudo apt-fast install nginx
sudo systemctl enable nginx
sudo systemctl restart nginx

# Install Nginx-Extras for headers (CORS)
sudo apt-fast install nginx-extras

# Install SuperVisor
sudo apt-fast install -y supervisor


# Install Rabbit MQ
# REFERENCE For configure rabbit MQ and to Add user:-
# http://docs.celeryproject.org/en/latest/getting-started/brokers/rabbitmq.html#installation-configuration
# NOTE :- This part is already done in set_up_project.sh

# Install Erlang First
# REFERENCE :- https://tecadmin.net/install-erlang-on-ubuntu/

wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb
sudo dpkg -i erlang-solutions_1.0_all.deb

sudo apt-fast update
sudo apt-fast install erlang

# To check the installation
# REFERENCE :- https://stackoverflow.com/questions/9560815/how-to-get-erlangs-release-version-number-from-a-shell
erl -eval '{ok, Version} = file:read_file(filename:join([code:root_dir(), "releases", erlang:system_info(otp_release), "OTP_VERSION"])), io:fwrite(Version), halt().' -noshell

# Or You can just type `erl` in shell, you'll see ErLang console. Press CTRL+C to exit the console


# Now install the RabbitMQ
# REFERENCE:- https://www.vultr.com/docs/how-to-install-rabbitmq-on-ubuntu-16-04-47

# Just follow the RabbitMq installation part, as we already installed the erlang earlier and if you try to install
# erlang from this tutorial than it will not be installed

# Add the Apt repository to your Apt source list directory (/etc/apt/sources.list.d):
echo "deb https://dl.bintray.com/rabbitmq/debian xenial main" | sudo tee /etc/apt/sources.list.d/bintray.rabbitmq.list

# Next add our public key to your trusted key list using apt-key:
wget -O- https://www.rabbitmq.com/rabbitmq-release-signing-key.asc | sudo apt-key add -

sudo apt-fast update
sudo apt-fast install rabbitmq-server

# Check the installation of rabbit MQ server run the following command
sudo systemctl status rabbitmq-server.service

# Start RabbitMQ as service
sudo systemctl enable rabbitmq-server.service
sudo systemctl restart rabbitmq-server.service

# Install Let's Encrypt

# Reference :- https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx

sudo apt-fast update
sudo apt-fast install software-properties-common
sudo add-apt-repository universe
sudo add-apt-repository ppa:certbot/certbot
sudo apt-fast update

sudo apt-fast install certbot python-certbot-nginx


# Install ZSH
# sudo apt-get install zsh
# chsh -s /bin/zsh
# exec zsh
sudo apt-fast install zsh
sh -c "$(wget -O- https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
