#!/usr/bin/env bash

# This will setup project on spograde
# Run this only after running the 'set_up_server.sh' script


# This will do the following thing

# - Create a postgres database along with a database user
# - Create a virtual environment in /opt/<project_name>/env
# - Clone the project from GitLab in /opt/<project_name>
# - Create and Run Migrations
# - Create Superuser for django-admin
# - Upload fixtures data and sheet data to database for initial data
# - Install Node Modules for frontend
# - Create a gunicorn config /etc/systemd/system/<conf_name>.service
# - Create a nginx config file /etc/nginx/sites-enabled/<conf_name>
# - Install SSL


# Change the directory to project root
# This folder will be created by previous script with necessary permissions

cd /opt/spograde


sudo su postgres
psql
CREATE DATABASE spograde;
CREATE USER spograde_user WITH PASSWORD 'spograde@password';
GRANT ALL PRIVILEGES ON DATABASE spograde TO spograde_user;
ALTER USER spograde WITH SUPERUSER;

\q

Press CTRL+D # Basically this will run `exit` command, we could write direct command here but it is changing pycharm
# theme color to white which makes our code unreadable hence we will press ctrl+D instead of running exit command


# NOTE ::: Run the below commands using ** devops2 **  user
# ======================================================================================================================

# Creating virtual environment

virtualenv -p python3 env
source env/bin/activate

# Cloning project from GitLab

git clone https://gitlab.com/rahuladream/spograde.git

cd spograde

# Install dependencies and libraries
pip install -r requirements.txt

# Create and Run Migrations
python manage.py makemigrations
python manage.py migrate

# Collect static files otherwise the static assets will not be loaded by the browser

python manage.py collectstatic --noinput


# Create log files to capture the logs from gunicorn
sudo mkdir /var/log/spograde

# Give permissions to devops group
#sudo chmod 777 -R /var/log/spograde/
sudo chgrp -R devops /var/log/spograde

sudo chmod -R 775 /var/log/spograde

# NOTE:- ===============================================================================================================
# ======================================================================================================================
# ======================================================================================================================
# If Backend didn't run than it might be issue with the logs permission, please try to change the permission with 777
# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================


touch /var/log/spograde/access.log
touch /var/log/spograde/error.log

# Create a gunicorn config /etc/systemd/system/<conf_name>.service

# TODO :- Try to run gunicorn with user dev-django1 and group webapps

# Set workers to 2*n +1 where n is the no. of processors
# Reference :- http://docs.gunicorn.org/en/stable/design.html#how-many-workers
# To find n run command `nproc`
# In our case n=2 so workers=5
sudo ln -s /opt/spograde/spograde/scripts/gunicorn_conf/spograde.service /etc/systemd/system/spograde.service

sudo systemctl start spograde
sudo systemctl enable spograde


# Create Celery configuration and setup Rabbit MQ

# FORMAT :- sudo rabbitmqctl add_user <user_name> <user_password>
sudo rabbitmqctl add_user spograde_user spograde@Password

# FORMAT :- sudo rabbitmqctl add_vhost <vhost(default= / )>
sudo rabbitmqctl add_vhost spograde

# FORMAT :- sudo rabbitmqctl set_permissions -p <vhost> <user_name> ".*" ".*" ".*"
sudo rabbitmqctl set_permissions -p spograde spograde_user ".*" ".*" ".*"

# Create a nginx config file /etc/nginx/sites-enabled/<conf_name>

sudo ln -s /opt/spograde/spograde/scripts/nginx_conf/spograde /etc/nginx/sites-enabled/spograde


# Restart nginx
sudo systemctl restart nginx


# Install SSL

sudo certbot --nginx

# For automatic renewal of certificates run cron jobs
# Refer this :- https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx
# See point No. 5 in the reference link



# Create supervisor configuration for celery worker and beat
# ----------------------------------------------------------------------------------------------------------------------

# Create the log files first

cd /var/log

sudo mkdir celery
cd celery
sudo touch spograde_worker_access.log
sudo touch spograde_worker_error.log
sudo touch spograde_beat_access.log
sudo touch spograde_beat_error.log


# Create configuration files now

sudo ln -s /opt/spograde/spograde/scripts/celery_conf/spograde_worker.conf /etc/supervisor/conf.d/spograde_worker.conf
sudo ln -s /opt/spograde/spograde/scripts/celery_conf/spograde_beat.conf /etc/supervisor/conf.d/spograde_beat.conf

# You can check the content of the file by typing cat <filename>

# Update changes for supervisor
sudo supervisorctl reread
sudo supervisorctl update

# Restart the worker and the beat if you want to, by default they are automatically started when you update the supervisor

sudo supervisorctl restart spograde_worker
sudo supervisorctl restart spograde_beat

# You can check the status by running the following command

sudo supervisorctl status spograde_worker
sudo supervisorctl status spograde_beat
