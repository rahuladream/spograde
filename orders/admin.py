from django.contrib import admin

from .models import Order, ProductPurchase


class AdminOrder(admin.ModelAdmin):
    list_display = ('order_id', 'cart', 'status', 'total', 'delhivery_awb_id', 'timestamp')
    list_filter = ('status', 'timestamp')
    search_fields = ('order_id', 'cart')


admin.site.register(Order, AdminOrder)


class AdminProductPurchase(admin.ModelAdmin):
    list_display = ('order_id', 'billing_profile', 'refunded', 'timestamp')


admin.site.register(ProductPurchase, AdminProductPurchase)
