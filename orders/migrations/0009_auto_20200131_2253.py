# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2020-01-31 17:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0008_delete_productpurchasemanager'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='shipping_total',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=100),
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(choices=[('created', 'Created'), ('paid', 'Paid'), ('picked', 'Picked by Courier'), ('shipped', 'On the way'), ('delivered', 'Order Delivered')], default='created', max_length=120),
        ),
    ]
