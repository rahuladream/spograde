from django.contrib import admin

from .models import ObjectViewed, UserSession, Visitors


class ObjectViewedAdmin(admin.ModelAdmin):
    list_display = ['user', 'ip_address', 'content_type', 'object_id', 'timestamp']
    list_filter = ('content_type',)


admin.site.register(ObjectViewed, ObjectViewedAdmin)


class UserSessionAdmin(admin.ModelAdmin):
    list_display = ('user', 'ip_address', 'timestamp')


admin.site.register(UserSession, UserSessionAdmin)


class VisitorAdmin(admin.ModelAdmin):
    list_display = ['ip_address', 'is_mobile', 'is_pc', 'hit_count', 'device']
    list_filter = ('is_mobile', 'is_pc')
    search_fields = ['ip_address', 'device', 'visitor_details']


admin.site.register(Visitors, VisitorAdmin)
