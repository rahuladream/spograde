from django.contrib import admin

from .models import Cart


class AdminCart(admin.ModelAdmin):
    list_display = ('user', 'total', 'timestamp')
    # ordering     = ('timestamp')
    search_fields = ('user', 'products')


admin.site.register(Cart, AdminCart)
