from django.conf.urls import url

from .views import (
    cart_home,
    cart_update,
    checkout_home,
    checkout_done_view,
    payment_response,
    coupon,
    special_checkout_home,
    special_cart,
    disable_coupon
)

urlpatterns = [
    url(r'^$', cart_home, name='home'),
    url(r'^checkout/success/$', checkout_done_view, name='success'),
    url(r'^supportyourteam/', special_cart, name='special_cart'),

    url(r'^checkout/$', checkout_home, name='checkout'),

    url(r'^supportyourteam-checkout/$', special_checkout_home, name='special_checkout'),

    url(r'^update/$', cart_update, name='update'),
    url(r'^apply_coupon/$', coupon, name='coupon'),
    url(r'^disable_coupon/$', disable_coupon, name='disable_coupon')

]
