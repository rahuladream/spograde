from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import render, redirect

from django.utils.translation import get_language

from accounts.forms import LoginForm, GuestForm, RegisterForm
from accounts.models import GuestEmail

from addresses.forms import AddressCheckoutForm
from addresses.models import Address

from billing.models import BillingProfile

from django.urls import reverse

import datetime

from django.http import HttpResponse, HttpResponseRedirect

from django.contrib import messages

from django.http import HttpResponse
import logging
from django.template import loader

from orders.models import Order

from paytm.models import PaytmHistory

from django.core.files.base import ContentFile

from spograde.constant import *

from accounts.models import User

from products.models import Product, ProductType, ProductSize
from .models import Cart

from django.conf import settings
# from .utils import render_to_pdf

from . import Checksum

from django.views.decorators.csrf import csrf_exempt
from post_office import mail
from django.template.loader import get_template

from django_simple_coupons.validations import validate_coupon
from django_simple_coupons.models import Coupon

import stripe

STRIPE_SECRET_KEY = getattr(settings, "STRIPE_SECRET_KEY", "sk_test_cu1lQmchLvYrSCp5XE")
STRIPE_PUB_KEY = getattr(settings, "STRIPE_PUB_KEY", 'pk_test_PrV61YZEeiYTTVMZ')
stripe.api_key = STRIPE_SECRET_KEY

PAYTM_FAILURE_DICT = ['00', '141', '153', '163', '196', '202', '205', '207', '208', '209', '210', '220', '222', '227',
                      '229', '232', '283', '294', '295', '296', '297', '302', '308', '309', '312', '314', '315', '316',
                      '317', '318', '319', '325', '330', '337', '372', '401', '402', '501', '504', '506', '509', '810',
                      '1001', '1006', '1007', '1008', '2023', '2271', '2272', '3102', '9999']


def cart_detail_api_view(request):
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    try:
        products = [{
            "id": x.id,
            "url": x.get_absolute_url(),
            "image": x.image.url,
            "name": x.name,
            "price": x.price,
            "p_type": "",
            "p_size": ""
        }
            for x in cart_obj.products.all()]
        p_type = [{
            "p_type": y.slug,
            "p_name": y.title,
            "p_price": y.mrp_price
        } for y in cart_obj.p_type.all()]
        p_size = [{
            "p_size": z.slug,
            "p_name": z.title
        } for z in cart_obj.p_size.all()]
        products[0]["p_type"] = p_type
        products[0]["p_size"] = p_size
    except IndexError:
        products = [{
            "id": x.id,
            "url": x.get_absolute_url(),
            "image": x.image.url,
            "name": x.name,
            "price": x.price,
            "p_type": "",
            "p_size": ""
        }
            for x in cart_obj.products.all()]
    cart_data = {"products": products, "subtotal": cart_obj.subtotal, "total": cart_obj.total}
    return JsonResponse(cart_data)


def cart_home(request):
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    return render(request, "carts/home.html", {"cart": cart_obj})


def cart_update(request):
    product_id = request.POST.get('product_id')
    product_type = request.POST.get('product_type')
    product_size = request.POST.get('product_size')
    if product_id is not None:
        try:
            product_obj = Product.objects.get(id=product_id)
        except Product.DoesNotExist:
            return redirect("cart:home")
        cart_obj, new_obj = Cart.objects.new_or_get(request)
        if product_obj in cart_obj.products.all():
            cart_obj.products.remove(product_obj)
            if cart_obj.products.all().count() == 0:
                cart_obj.delete()
            added = False
        else:
            product_type = ProductType.objects.get(slug=product_type) if product_type is not None else None
            product_size = ProductSize.objects.get(slug=product_size) if product_size is not None else None
            cart_obj.products.add(product_obj)  # cart_obj.products.add(product_id)
            cart_obj.p_type.add(product_type) if product_type is not None else None
            cart_obj.p_size.add(product_size) if product_size is not None else None
            added = True
        # import pdb; pdb.set_trace();
        request.session['cart_items'] = 0 if str(cart_obj) == 'None' else cart_obj.products.count()
        # return redirect(product_obj.get_absolute_url())

        if request.is_ajax():  # Asynchronous JavaScript And XML / JSON
            json_data = {
                "added": added,
                "removed": not added,
                "cartItemCount": 0 if str(cart_obj) == 'None' else cart_obj.products.count()
            }
            return JsonResponse(json_data, status=200)  # HttpResponse
            # return JsonResponse({"message": "Error 400"}, status=400) # Django Rest Framework
    return redirect("cart:home")


def coupon(request):
    code = request.POST.get('code')
    user = User.objects.get(email=request.user.email)

    status = validate_coupon(coupon_code=code, user=user)

    # two coupan code returned

    if status['valid']:
        coupon = Coupon.objects.get(code=code)
        coupon.use_coupon(user=user)
        cart_obj, new_obj = Cart.objects.new_or_get(request)
        discount_value = coupon.get_discounted_value(initial_value=cart_obj.total)
        cart_obj.is_coupon = True
        cart_obj.previous_price = cart_obj.total
        cart_obj.subtotal = discount_value
        cart_obj.coupon_code = coupon
        cart_obj.save()
        request.session['is_applied'] = True
        request.session['code'] = request.POST.get('code')
        messages.success(request, '{} coupon has been applied ✅.'.format(request.POST.get('code')))
    else:
        request.session['is_applied'] = False
        messages.error(request, status['message'])

    # messages.error(request, 'Oops, code is invalid')
    return redirect("cart:checkout")


def disable_coupon(request):
    cart_obj, new_or_get = Cart.objects.new_or_get(request)
    # once coupan applied and removed than coupan must be reusable
    cart_obj.is_coupon = False
    cart_obj.subtotal = cart_obj.previous_price
    cart_obj.save()
    messages.success(request, 'Coupon code removed 🗙.')
    return redirect("cart:checkout")


import decimal


def special_cart(request):
    product_id = request.POST.get('product_id')
    product_size = request.POST.get('product_size')
    product_type = request.POST.get('product_type')

    cart_obj, new_or_get = Cart.objects.new_or_get(request)
    product_type = ProductType.objects.get(slug=product_type) if product_type is not None else None
    product_size = ProductSize.objects.get(slug=product_size) if product_size is not None else None
    cart_obj.products.add(Product.objects.get(id=product_id))  # cart_obj.products.add(product_id)
    cart_obj.p_type.add(product_type) if product_type is not None else None
    cart_obj.p_size.add(product_size) if product_size is not None else None

    cart_obj.subtotal = decimal.Decimal(SPECIAL_PRICE)
    cart_obj.total = decimal.Decimal(SPECIAL_PRICE)
    cart_obj.save()

    return redirect("cart:special_checkout")


def special_checkout_home(request):
    payment_type = 'paytm'
    cart_obj, cart_created = Cart.objects.new_or_get(request)
    order_obj = None
    if cart_created or cart_obj.products.count() == 0:
        return redirect("cart:home")

    login_form = LoginForm(request=request)
    register_form = RegisterForm()
    guest_form = GuestForm(request=request)
    address_form = AddressCheckoutForm()
    billing_address_id = request.session.get("billing_address_id", None)

    shipping_address_required = not cart_obj.is_digital

    shipping_address_id = request.session.get("shipping_address_id", None)

    billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)
    address_qs = None
    has_card = True
    if billing_profile is not None:
        if request.user.is_authenticated():
            address_qs = Address.objects.filter(billing_profile=billing_profile)
        order_obj, order_obj_created = Order.objects.new_or_get(billing_profile, cart_obj)
        if shipping_address_id:
            order_obj.shipping_address = Address.objects.get(id=shipping_address_id)
            del request.session["shipping_address_id"]
        if billing_address_id:
            order_obj.billing_address = Address.objects.get(id=billing_address_id)
            del request.session["billing_address_id"]
        if billing_address_id or shipping_address_id:
            order_obj.save()
        has_card = billing_profile.has_card
    if request.method == "POST":
        "check that order is done"
        if payment_type == 'paytm':

            is_prepared = order_obj.check_done()
            if is_prepared:
                MERCHANT_KEY = settings.PAYTM_MERCHANT_KEY
                MERCHANT_ID = settings.PAYTM_MERCHANT_ID
                get_lang = "/" + get_language() if get_language() else ''
                CALLBACK_URL = settings.HOST_URL + settings.PAYTM_CALLBACK_URL
                if cart_obj.subtotal:
                    order_pay_dict = {
                        'MID': MERCHANT_ID,
                        'ORDER_ID': order_obj.order_id,
                        'TXN_AMOUNT': str(cart_obj.subtotal),
                        'CUST_ID': request.user.email,
                        'INDUSTRY_TYPE_ID': settings.RETAIL,
                        'WEBSITE': settings.PAYTM_WEBSITE,
                        'CHANNEL_ID': settings.CHANNEL,
                        'CALLBACK_URL': CALLBACK_URL
                    }
                    param_dict = order_pay_dict
                    param_dict['CHECKSUMHASH'] = Checksum.generate_checksum(order_pay_dict, MERCHANT_KEY)
            return render(request, "carts/payment.html", {'paytmdict': param_dict})
        else:
            return redirect("cart:checkout")
    context = {
        "object": order_obj,
        'cart': cart_obj,
        'special': True,
        "billing_profile": billing_profile,
        "login_form": login_form,
        "register_form": register_form,
        "guest_form": guest_form,
        "address_form": address_form,
        "address_qs": address_qs,
        "has_card": has_card,
        "publish_key": STRIPE_PUB_KEY,
        "shipping_address_required": shipping_address_required,
    }
    return render(request, "carts/checkout.html", context)


def checkout_home(request):
    cart_obj, cart_created = Cart.objects.new_or_get(request)
    order_obj = None
    if cart_created or cart_obj.products.count() == 0:
        return redirect("cart:home")

    login_form = LoginForm(request=request)
    register_form = RegisterForm()
    guest_form = GuestForm(request=request)
    address_form = AddressCheckoutForm()
    billing_address_id = request.session.get("billing_address_id", None)

    shipping_address_required = not cart_obj.is_digital

    shipping_address_id = request.session.get("shipping_address_id", None)

    billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)
    address_qs = None
    has_card = True
    if billing_profile is not None:
        if request.user.is_authenticated():
            address_qs = Address.objects.filter(billing_profile=billing_profile)
        order_obj, order_obj_created = Order.objects.new_or_get(billing_profile, cart_obj)
        if shipping_address_id:
            order_obj.shipping_address = Address.objects.get(id=shipping_address_id)
            del request.session["shipping_address_id"]
        if billing_address_id:
            order_obj.billing_address = Address.objects.get(id=billing_address_id)
            del request.session["billing_address_id"]
        if billing_address_id or shipping_address_id:
            order_obj.save()
        has_card = billing_profile.has_card
    if request.method == "POST":
        "check that order is done"
        payment_type = request.POST.get('pay_type')

        if payment_type == 'paytm':

            print('paytm method called')

            is_prepared = order_obj.check_done()
            if is_prepared:
                MERCHANT_KEY = settings.PAYTM_MERCHANT_KEY
                MERCHANT_ID = settings.PAYTM_MERCHANT_ID
                get_lang = "/" + get_language() if get_language() else ''
                CALLBACK_URL = settings.HOST_URL + settings.PAYTM_CALLBACK_URL
                if cart_obj.subtotal:
                    order_pay_dict = {
                        'MID': MERCHANT_ID,
                        'ORDER_ID': order_obj.order_id,
                        'TXN_AMOUNT': str(cart_obj.subtotal),
                        'CUST_ID': request.user.email,
                        'INDUSTRY_TYPE_ID': settings.RETAIL,
                        'WEBSITE': settings.PAYTM_WEBSITE,
                        'CHANNEL_ID': settings.CHANNEL,
                        'CALLBACK_URL': CALLBACK_URL
                    }
                    param_dict = order_pay_dict
                    param_dict['CHECKSUMHASH'] = Checksum.generate_checksum(order_pay_dict, MERCHANT_KEY)
            return render(request, "carts/payment.html", {'paytmdict': param_dict})
            # Some process
        elif payment_type == 'cod':
            is_prepared = order_obj.check_done()
            if is_prepared:
                context = {}
                order_obj.mark_cod()  # sort a signal for us
                request.session['cart_items'] = 0
                del request.session['cart_id']
                try:
                    base_url = getattr(settings, 'BASE_URL', 'https://spograde.com')
                    now = datetime.datetime.now()
                    context = {
                        'order_number': order_obj.order_id,
                        'product_name': cart_obj.products.all(),
                        'total_cost': order_obj.cart.subtotal,
                        'shipping_handling': 0,
                        'sales_tax': 0,
                        'payment_method': 'Cash on Delivery',
                        'delivery_address': order_obj.shipping_address_final,
                        'estimated_delivery': order_obj.timestamp,
                        'invoice_date': now.strftime("%d/%m/%y"),
                    }
                    txt_ = get_template("order_email/sucess_order_email.txt").render(context)
                    html_ = get_template("order_email/sucess_order_email.html").render(context)
                    subject = "We've recieved your order - Spograde"
                    from_email = settings.EMAIL_HOST_USER
                    recipient_list = [request.user.email]
                    mail.send(
                        recipient_list,
                        from_email,
                        subject=subject,
                        message=txt_,
                        html_message=html_,
                        headers={'Reply-to': 'spograde@gmail.com'},
                        priority='high'
                    )
                except Exception as e:
                    logging.error("Exception while sending final mail :{}".format(e), exc_info=True)
            return render(request, 'carts/checkout-done.html', {'payment_response': context})
        else:
            return redirect("cart:checkout")
    context = {
        "object": order_obj,
        'cart': cart_obj,
        "billing_profile": billing_profile,
        "login_form": login_form,
        "register_form": register_form,
        "guest_form": guest_form,
        "address_form": address_form,
        "address_qs": address_qs,
        "has_card": has_card,
        "publish_key": STRIPE_PUB_KEY,
        "shipping_address_required": shipping_address_required,
    }
    return render(request, "carts/checkout.html", context)


@csrf_exempt
def payment_response(request):
    if request.method == "POST":
        MERCHANT_KEY = settings.PAYTM_MERCHANT_KEY
        response_dict = {}
        for key in request.POST:
            response_dict[key] = request.POST[key]
        verify = Checksum.verify_checksum(response_dict, MERCHANT_KEY, response_dict['CHECKSUMHASH'])
        order_obj = Order.objects.get(order_id=request.POST.get('ORDERID'))
        cart_obj = Cart.objects.get(pk=order_obj.cart.pk)
        if verify:
            if not request.POST.get('TXNID'):
                TXNID = 'TXNID not provided from Paytm'
            else:
                TXNID = request.POST.get('TXNID')
            PaytmHistory.objects.create(
                user=User.objects.get(email=request.user.email),
                CURRENCY=request.POST.get('CURRENCY'),
                GATEWAYNAME=request.POST.get('GATEWAYNAME'),
                RESPMSG=request.POST.get('RESPMSG'),
                BANKNAME=request.POST.get('BANKNAME'),
                PAYMENTMODE=request.POST.get('PAYMENTMODE'),
                MID=request.POST.get('MID'),
                RESPCODE=request.POST.get('RESPCODE'),
                TXNID=TXNID,
                TXNAMOUNT=request.POST.get('TXNAMOUNT'),
                order=order_obj,
                STATUS=request.POST.get('STATUS'),
                BANKTXNID=request.POST.get('BANKTXNID'),
            )
            if request.POST.get('RESPCODE') == '01':
                order_obj.mark_paid()  # sort a signal for us
                request.session['cart_items'] = 0
                del request.session['cart_id']

                try:
                    base_url = getattr(settings, 'BASE_URL', 'https://spograde.com')
                    context = {
                        'order_number': order_obj.order_id,
                        'product_name': cart_obj.products.first().title,
                        'total_cost': order_obj.cart.subtotal,
                        'shipping_handling': 0,
                        'sales_tax': 0,
                        'payment_method': 'Online Payment (Paytm)',
                        'delivery_address': order_obj.shipping_address_final,
                        'estimated_delivery': order_obj.timestamp,
                    }
                    txt_ = get_template("order_email/sucess_order_email.txt").render(context)
                    html_ = get_template("order_email/sucess_order_email.html").render(context)
                    subject = "We've recieved your order - Spograde"
                    from_email = settings.EMAIL_HOST_USER
                    recipient_list = [request.user.email]
                    mail.send(
                        recipient_list,
                        from_email,
                        subject=subject,
                        message=txt_,
                        html_message=html_,
                        headers={'Reply-to': 'spograde@gmail.com'},
                        priority='high',
                    )
                except Exception as e:
                    pass
                return render(request, 'carts/checkout-done.html', {'payment_response': response_dict})
            else:
                order_obj.mark_fail()  # sort a signal for us
                request.session['cart_items'] = 0
                del request.session['cart_id']
                base_url = getattr(settings, 'BASE_URL', 'https://spograde.com')
                context = {
                    'order_number': order_obj.order_id,
                    'product_name': cart_obj.products.all(),
                    'total_cost': order_obj.cart.subtotal,
                    'shipping_handling': 0,
                    'sales_tax': 0,
                    'delivery_address': order_obj.shipping_address_final,
                    'estimated_delivery': order_obj.timestamp,
                    'mid': request.POST.get('MID'),
                    'respmsg': request.POST.get('RESPMSG')
                }
                txt_ = get_template("order_email/fail_order_email.txt").render(context)
                html_ = get_template("order_email/fail_order_email.html").render(context)
                subject = "Your transaction has not been completed - Spograde"
                from_email = settings.EMAIL_HOST_USER
                recipient_list = [request.user.email]
                mail.send(
                    recipient_list,
                    from_email,
                    subject=subject,
                    message=txt_,
                    html_message=html_,
                    headers={'Reply-to': 'spograde@gmail.com'},
                    priority='high',
                )

                return render(request, 'carts/checkout-fail.html',
                              context={'respcode': request.POST.get('RESPCODE'), 'mid': request.POST.get('MID'),
                                       'respmsg': request.POST.get('RESPMSG')})
        else:
            return render(request, 'carts/checkout-fail.html')
    return HttpResponse(status=200)


def checkout_done_view(request):
    return render(request, "carts/checkout-done.html", {})
