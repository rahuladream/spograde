# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2020-03-07 14:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0036_auto_20200307_2007'),
        ('carts', '0003_auto_20200214_0031'),
    ]

    operations = [
        migrations.AddField(
            model_name='cart',
            name='p_size',
            field=models.ManyToManyField(blank=True, null=True, to='products.ProductSize'),
        ),
        migrations.AddField(
            model_name='cart',
            name='p_type',
            field=models.ManyToManyField(blank=True, null=True, to='products.ProductType'),
        ),
    ]
