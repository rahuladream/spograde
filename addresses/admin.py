from django.contrib import admin

from .models import Address


class AdminAddress(admin.ModelAdmin):
    list_display = ('nickname', 'name', 'address_type', 'city', 'country', 'postal_code')
    list_filter = ('city', 'country')
    search_fields = ('address_line_1', 'address_line_2')


admin.site.register(Address, AdminAddress)
